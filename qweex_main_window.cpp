#include "qweex_main_window.h"

Qweex::MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    about_window(new Qweex::AboutWindow(this))
{
    #ifdef SUPPORT_THE_DEV
    Purchase::readLicenseInfo();
    #endif
}

void Qweex::MainWindow::doPlatformSpecific()
{
    if(done_platform_specific)
        return;
    #ifdef __APPLE__
    setWindowIcon(QIcon());
    setUnifiedTitleAndToolBarOnMac(true);
    QMenu* Window = new QMenu("Window");
    {
        QAction* minimize = new QAction(tr("Minimize"),Window);
        minimize->setShortcut(tr("Ctrl+M"));
        connect(minimize,SIGNAL(triggered()),this,SLOT(showMinimized()));
        Window->addAction(minimize);
    }
    {
        QAction* zoom = new QAction(tr("Zoom"), Window);
        connect(zoom,SIGNAL(triggered()),this,SLOT(onZoomMenuItem()));
        Window->addAction(zoom);
    }
    QList<QMenu*> menus = menuBar()->findChildren<QMenu*>();
    if(menus.length()==0)
        menuBar()->addMenu(Window);
    else {
        if(menus.last()==Help)
            menuBar()->insertMenu(menuBar()->actions().last(), Window);
        else
            menuBar()->addMenu(Window);
    }
    #endif
    done_platform_specific = true;
}

void Qweex::MainWindow::addMenus() {
    if(added_menus)
        return;
    Help = new QMenu(tr("Help"));
    {
        QAction* about = new QAction(tr("About"), Help);
        connect(about, SIGNAL(triggered()), about_window,SLOT(show()));
        Help->addAction(about);
    }
    {
        QAction* license = new QAction(tr("License"), Help);
        connect(license, SIGNAL(triggered()), about_window, SLOT(showLicense()));
        Help->addAction(license);
    }
    this->menuBar()->addMenu(Help);

    #ifdef SUPPORT_THE_DEV
    if(Purchase::keyIsValid())
    {
        QAction* key = new QAction(tr("View License Info"),Help);
        connect(key,SIGNAL(triggered()),this,SLOT(showLicense()));
        Help->addAction(key);
    }
    else
    {
        QMenu * devMenu = new QMenu(tr("Support the Dev"));
        {
            QAction* buy = new QAction(tr("Buy"),devMenu);
            connect(buy,SIGNAL(triggered()),this,SLOT(buy()));
            devMenu->addAction(buy);
        }
        {
            QAction* key = new QAction(tr("Enter License Key"),devMenu);
            qDebug() << "Poop";
            connect(key,SIGNAL(triggered()),this,SLOT(enterLicense()));
            devMenu->addAction(key);
        }
        this->menuBar()->addMenu(devMenu);
    }
    #endif
    added_menus = true;
}

#ifdef SUPPORT_THE_DEV
void Qweex::MainWindow::receivedLicenseConfirmation()
{
    QAction *toRemove = menuBar()->actions().back();
    this->menuBar()->removeAction(toRemove);

    QMenu* Help = (QMenu*)this->menuBar()->actions().back()->parent();
    QAction* key = new QAction(tr("View License Info"),Help);
    connect(key, SIGNAL(triggered()), this, SLOT(showLicense()));
    Help->addAction(key);
    //TODO: .....WHY the fuck did I write "TODO" here ages ago?
}
#endif
