SOURCES += $$PWD/qweex_main_window.cpp $$PWD/about_window.cpp

HEADERS += $$PWD/qweex_main_window.h $$PWD/about_window.h $$PWD/purchase.h.sample

OTHER_FILES += $$PWD/README.md \
                $$PWD/LICENSE \
                $$PWD/project.h.sample

exists($$PWD/purchase/purchase.h) {
    message("Including evil proprietary stuff that I'm going to hell for")
    DEFINES += "SUPPORT_THE_DEV"
    HEADERS += $$PWD/purchase/*.h
    SOURCES += $$PWD/purchase/*cpp
    QT += network
}

RESOURCES += $$PWD/qweex.qrc
